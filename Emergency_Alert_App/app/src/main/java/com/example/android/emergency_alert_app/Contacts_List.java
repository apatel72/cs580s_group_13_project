package com.example.android.emergency_alert_app;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Contacts_List extends AppCompatActivity {

    public final static String EXTRA_RETURN_MESSAGE =
            "com.example.android.emergency_alert_app.RETURN_MESSAGE";


    public static final int TEXT_REQUEST = 3;

    ArrayList<String> contacts_array = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts__list);

        //save contact button
        Button save_contact = (Button) findViewById(R.id.save_contact);
        //input text box
        final EditText textinput = (EditText) findViewById(R.id.textinput);
        //listview
        final ListView show_contacts = (ListView) findViewById(R.id.show_contacts);

        save_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputfromtextbox = textinput.getText().toString();

                if(contacts_array.contains(inputfromtextbox)){
                    Toast.makeText(getBaseContext(), "Contact already exists!!!", Toast.LENGTH_LONG).show();
                }
                else if (inputfromtextbox == null || inputfromtextbox.trim().equals("")){
                    Toast.makeText(getBaseContext(), "Nothing is entered!!!", Toast.LENGTH_LONG).show();
                }
                else{
                    contacts_array.add(inputfromtextbox);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Contacts_List.this, android.R.layout.simple_list_item_1, contacts_array);
                    show_contacts.setAdapter(adapter);
                    ((EditText)findViewById(R.id.textinput)).setText("");
                }

            }
        });


        main_page_method();





    }

    public void main_page_method(){

        Button button_goto_main = (Button) findViewById(R.id.button_goto_main);
        button_goto_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnContactsIntent = new Intent();

                returnContactsIntent.putExtra(EXTRA_RETURN_MESSAGE, contacts_array);


                setResult(RESULT_OK, returnContactsIntent);
                //to finish the activity and go back to main page
                finish();
            }
        });


    }
}
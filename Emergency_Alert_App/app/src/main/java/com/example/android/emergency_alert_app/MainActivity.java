/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.emergency_alert_app;

import android.Manifest;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;

import com.google.android.gms.location.*;
import com.google.android.gms.tasks.*;

import android.provider.ContactsContract;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements
        FetchAddressTask.OnTaskCompleted {


    public TextView voiceToText;
<<<<<<< HEAD
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
   // private ShakeDetector mShakeDetector;
=======
>>>>>>> 67614d7e6e8b02db68a272a90f2edf75ede2e1b2

    public static final int REQUEST_LOCATION_PERMISSION = 1;
    private static final String TAG = "MainActivity";
    Location mLastLocation;
    FusedLocationProviderClient mFusedLocationClient;
    TextView mLocationTextView;
    private boolean mTrackingLocation;
    public static final int CONTACT_DATA = 7;

    public static final int TEXT_REQUEST = 3;

    ArrayList<String> processed_data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button mLocationButton = (Button) findViewById(R.id.button_location);

        mLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLocation();
            }
        });

        mLocationTextView = (TextView) findViewById(R.id.textview_location);
        //FusedLocationProviderClient mFusedLocationClient;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        //initialize textview for voice input
        voiceToText = (TextView) findViewById(R.id.voiceToText);


<<<<<<< HEAD
        // ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
                /*
                 * The following method, "handleShakeEvent(count):" is a stub //
                 * method you would use to setup whatever you want done once the
                 * device has been shook.
                 */
                handleShakeEvent(count);
=======
>>>>>>> 67614d7e6e8b02db68a272a90f2edf75ede2e1b2
        //contacts button method
        contacts_list_method();

    }

    public void contacts_list_method()
    {
        Button contacts_list = (Button) findViewById(R.id.contacts_list);
        contacts_list.setOnClickListener(new View.OnClickListener() {
            //@Override
            Intent i2 = new Intent(MainActivity.this, Contacts_List.class);
            public void onClick(View v) {
                startActivityForResult(i2 , TEXT_REQUEST);
            }
        });
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                            {Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        } else {
            //Log.d(TAG, "getLocation: permissions granted");
            mFusedLocationClient.getLastLocation().addOnSuccessListener(
                    new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                // Start the reverse geocode AsyncTask
                                new FetchAddressTask(MainActivity.this,
                                        MainActivity.this).execute(location);
                            } else {
                                mLocationTextView.setText(R.string.no_location);
                            }
                        }
                    });
        }
        mLocationTextView.setText(getString(R.string.address_text,
                getString(R.string.loading),
                System.currentTimeMillis()));
        //smsSendMessage();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                // If the permission is granted, get the location,
                // otherwise, show a Toast
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    Toast.makeText(this,
                            "location permission denied",
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    @Override
    public void onTaskCompleted(String result) {

        // Update the UI
        mLocationTextView.setText(getString(R.string.address_text,
                result, System.currentTimeMillis()));
        smsSendMessage();
    }

    //method for voice input
    public void getVoiceInput(View view) {
//implicit intent to take user input(voice)
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault()); //even if commented, takes devices default lang

        if(intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        }
        else{
            Toast.makeText(this, "Smartphone doesnt take speech input!!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode)
        {
            case 10:
                if(resultCode == RESULT_OK && data != null)
                {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    voiceToText.setText(result.get(0));
                    //String result_String = result.toString();

                    if(result.contains("Danger") | result.contains("danger") | result.contains("DANGER") |
                            result.contains("help") | result.contains("Help") | result.contains("HELP")    )
                    {
                        getLocation();

                    }

                    //voiceToText.setText("Ankit Patel");
                }
                else
                {
                    voiceToText.setText("Ankit Patel");
                }
                break;
            default:
                voiceToText.setText("requestCode is " + requestCode +"resultCode is " +resultCode + "and RESULT_OK is " +RESULT_OK);
                break;
        }



        if (requestCode == TEXT_REQUEST) {
            if (resultCode == RESULT_OK) {

                processed_data = data.getStringArrayListExtra(Contacts_List.EXTRA_RETURN_MESSAGE);
                //data.getStringExtra(Contacts_List.EXTRA_RETURN_MESSAGE);
                // process data
            }
        }







    }




    public void smsSendMessage() {
        // Set the phone number to the string
        //String destinationAddress = "16073740625";

        //ArrayList<String> destinationAddress1 = new ArrayList<String>(Arrays.asList("16073740625", "16073520315"));
        // Get the text of the sms message.

        String smsMessage = "Help Needed!!!!\n" + mLocationTextView.getText().toString();

        // Sets the SMSC(short msg service center) address, default = null.
        String scAddress = null;
        //to make sure msg is sent or delivered
        PendingIntent sentIntent = null, deliveryIntent = null;

        // Using SmsManager class.
        SmsManager smsManager = SmsManager.getDefault();
        Log.d("default sms manager", smsMessage);

        for(int i = 0; i < processed_data.size() ;i++) {
            smsManager.sendTextMessage(processed_data.get(i), scAddress, smsMessage, sentIntent, deliveryIntent);
        }
    }




}